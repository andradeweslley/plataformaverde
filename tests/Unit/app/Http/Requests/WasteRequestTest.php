<?php
namespace App\Http\Requests;

use Tests\TestCase;

class WasteRequestTest extends TestCase
{
    public function testShouldAuthorizeAnyRequest()
    {
        // Set
        $formRequest = WasteRequest::create('/');

        // Actions
        $result = $formRequest->authorize();

        // Assertions
        $this->assertTrue($result);
    }

    public function testRules()
    {
        // Set
        $formRequest = WasteRequest::create('/');

        // Actions
        $result = $formRequest->rules();

        // Assertions
        $this->assertNotEmpty($result);
    }
}
