<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('original_name', 255);
            $table->string('path', 255);
            $table->integer('status')->default(0);
            $table->text('process_return')->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('waste', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_id');
            $table->string('common_name', 255);
            $table->string('type', 255);
            $table->string('category', 255);
            $table->string('treatment_technology', 255);
            $table->string('class', 255);
            $table->string('unit_measurement', 255);
            $table->string('weight', 255);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
        Schema::dropIfExists('waste');
    }
}
