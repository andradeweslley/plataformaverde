<?php

namespace App\Providers;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class SqliteServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $databaseFile = config('database.connections.sqlite.database');
        if (!file_exists($databaseFile)) {
            if (!file_exists(database_path())) {
                info('Creating Sqlite Path "'.database_path().'"');

                mkdir(database_path());
            }

            info('Creating Sqlite File "'.$databaseFile.'"');
            file_put_contents($databaseFile, '');

            info('Running migrations');
            Artisan::call('migrate', ['--force' => true]);
        }
    }
}
