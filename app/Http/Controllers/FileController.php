<?php
namespace App\Http\Controllers;

use App\Http\Requests\FileRequest;
use App\Http\Models\FileModel;
use App\Jobs\ProcessFile;
use Illuminate\Http\JsonResponse;

class FileController extends Controller
{
    public function getAll(): JsonResponse
    {
        return new JsonResponse(
            FileModel::all([
                'id',
                'original_name',
                'status',
                'process_return',
                'created_at',
                'updated_at',
            ])
        );
    }

    public function getOne(int $id): JsonResponse
    {
        $file = FileModel::find($id, [
            'id',
            'original_name',
            'status',
            'process_return',
            'created_at',
            'updated_at',
        ]);

        abort_if(empty($file), 404, 'Not Found');

        return new JsonResponse($file);
    }

    public function post(FileRequest $request): JsonResponse
    {
        $file = $request->file('file');

        if (!$file->isValid() || 'xlsx' !== strtolower($file->extension())) {
            return new JsonResponse(
                [
                    'file' => ['The file must be a XLSX.'],
                ],
                422
            );
        }

        $fileName = $file->getClientOriginalName();

        // Save file
        $fileNameInDisk = $request->file('file')->store('spreadsheet');

        if (false !== $fileNameInDisk) {
            $fileModel = new FileModel();

            $fileModel->original_name = $fileName;
            $fileModel->path = $fileNameInDisk;

            if ($fileModel->save()) {
                // Start Job
                ProcessFile::dispatch($fileNameInDisk, $fileModel->id);

                return new JsonResponse(
                    [
                        'message' => 'Success',
                        'file' => $fileName,
                    ]
                );
            }
        }

        return new JsonResponse(
            [
                'message' => 'Error',
                'file' => $fileName,
            ],
            500
        );
    }
}
