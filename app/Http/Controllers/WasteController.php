<?php
namespace App\Http\Controllers;

use App\Http\Models\WasteModel;
use App\Http\Requests\WasteRequest;
use Illuminate\Http\JsonResponse;
use phpDocumentor\Reflection\Types\Object_;

class WasteController extends Controller
{
    public function getAll(): JsonResponse
    {
        return new JsonResponse(
            WasteModel::all([
                'id',
                'common_name',
                'type',
                'category',
                'treatment_technology',
                'class',
                'unit_measurement',
                'weight',
                'created_at',
                'updated_at',
            ])
        );
    }

    public function getOne(int $id): JsonResponse
    {
        $waste = $this->getWasteById($id);

        return new JsonResponse($waste);
    }

    public function put(WasteRequest $request, int $id): JsonResponse
    {
        $waste = $this->getWasteById($id);

        $waste->common_name = $request->get('common_name');
        $waste->type = $request->get('type');
        $waste->category = $request->get('category');
        $waste->treatment_technology = $request->get('treatment_technology');
        $waste->class = $request->get('class');
        $waste->unit_measurement = $request->get('unit_measurement');
        $waste->weight = $request->get('weight');

        $waste->save();

        return new JsonResponse(['message' => 'Success']);
    }

    public function delete(int $id): JsonResponse
    {
        $waste = $this->getWasteById($id);

        $waste->delete();

        return new JsonResponse(['message' => 'Success']);
    }

    private function getWasteById(int $id)
    {
        $waste = WasteModel::find($id, [
            'id',
            'common_name',
            'type',
            'category',
            'treatment_technology',
            'class',
            'unit_measurement',
            'weight',
            'created_at',
            'updated_at',
        ]);

        abort_if(empty($waste), 404, 'Not Found');

        return $waste;
    }
}
