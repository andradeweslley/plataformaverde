<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WasteModel extends Model
{
    use SoftDeletes;

    protected $table = 'waste';
    protected $fillable = [
        'file_id',
        'common_name',
        'type',
        'category',
        'treatment_technology',
        'class',
        'unit_measurement',
        'weight',
    ];

    public function file()
    {
        $this->belongsTo('App\Http\Models\File');
    }
}
