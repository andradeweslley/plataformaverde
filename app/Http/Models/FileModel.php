<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileModel extends Model
{
    use SoftDeletes;

    protected $table = 'files';
    protected $fillable = [
        'original_name',
        'path',
        'status',
        'process_return'
    ];

    public function waste()
    {
        return $this->hasMany('App\Http\Models\Waste');
    }
}
