<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WasteRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'common_name' => ['string'],
            'type' => ['string'],
            'category' => ['string'],
            'treatment_technology' => ['string'],
            'class' => ['string'],
            'unit_measurement' => ['string'],
            'weight' => ['string'],
        ];
    }
}
