<?php

namespace App\Imports;

use App\Http\Models\WasteModel;
use Maatwebsite\Excel\Concerns\ToModel;

class WasteImport implements ToModel
{
    protected $idFile;

    public function __construct(int $id)
    {
        $this->idFile = $id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!array_filter($row) || 'Nome Comum do Resíduo' === $row[0]) {
            return null;
        }

        return new WasteModel([
            'file_id' => $this->idFile,
            'common_name' => $row[1],
            'type' => $row[2],
            'category' => $row[3],
            'treatment_technology' => $row[4],
            'class' => $row[5],
            'unit_measurement' => $row[6],
            'weight' => $row[7],
        ]);
    }
}
