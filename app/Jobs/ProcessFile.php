<?php

namespace App\Jobs;

use App\Http\Models\FileModel;
use App\Imports\WasteImport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;


class ProcessFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fileName;
    protected $id;

    public function __construct(string $fileName, int $id)
    {
        $this->fileName = $fileName;
        $this->id = $id;
    }

    public function handle()
    {
        $fileModel = FileModel::find($this->id);

        if (!empty($fileModel)) {
            $codeStatus = 2; // ERROR

            $returnObj = [
                'status' => ''
            ];

            try {
                Excel::import(new WasteImport($this->id), $this->fileName);

                $returnObj['status'] = 'Processed';
                $codeStatus = 1; // SUCCESS
            } catch (\Exception $e) {
                $returnObj['status'] = 'Error to process file';
            }

            $fileModel->status = $codeStatus;
            $fileModel->process_return = json_encode($returnObj);

            $fileModel->save();
        }
    }
}
