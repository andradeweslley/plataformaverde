<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Controllers\FileController;
use App\Http\Controllers\WasteController;

Route::group(['prefix' => 'v1'], function () {
    Route::get('documentation.yml', function () {
        return response()->view(
            'documentation',
            [],
            200,
            ['Content-Type' => 'text/yaml; charset=UTF-8']
        );
    });

    Route::group(['prefix' => 'file'], function () {
        Route::post('/', [FileController::class, 'post']);
        Route::get('/', [FileController::class, 'getAll']);
        Route::get('{id}', [FileController::class, 'getOne']);
    });

    Route::group(['prefix' => 'waste'], function () {
        Route::get('/', [WasteController::class, 'getAll']);
        Route::get('{id}', [WasteController::class, 'getOne']);
        Route::put('{id}', [WasteController::class, 'put']);
        Route::delete('{id}', [WasteController::class, 'delete']);
    });
});
