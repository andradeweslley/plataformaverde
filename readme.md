## Eu

**Nome:** `Weslley Araujo`

**E-mail:** `weslley.araujo2006@gmail.com`

**Usuário:** @andradeweslley

## Explicando

Esse repositório foi criado com base com PHP 7.3, Laravel 5.8 e Docker.
Foi configurado o banco de dados SQLite, não é necessário fazer nenhum processo, o próprio sistema cria o banco e as tabelas.


É necessário ter o [Docker](https://www.docker.com/get-started) e o [Docker Compose](https://docs.docker.com/compose/) instalados.
Utilize o `docker-compose` para subir o projeto.
Garanta que a porta `80` de sua máquina não esteja sendo utilizada e rode o comando abaixo:

```bash
docker-compose up -d
```

Em seguida, será necessário instalar as dependências do projeto:

```bash
docker-compose exec web composer setup
```

A partir daqui, está tudo configurado :rocket:

Assim, será possível acessar [http://localhost](http://localhost).

Os recursos da API estão disponíveis em [http://localhost/api/v1](http://localhost/api/v1). 

Não foi feita a interface para consumo da API. Porém para facilitar há uma [collection do postman](Plataforma_Verde_Test.postman_collection.json) no projeto.

## Recursos

No projeto foram criados dois recursos. O recurso `files` e `waste`.

O recurso `files` contém os métodos `POST` para realizar o envio dos arquivos e `GET` para buscar os arquivos enviados.

Os status possíveis são:
- **0** - Não processado.
- **1** - Processado com sucesso.
- **2** - Ocorreu um erro no processamento.

O recurso `waste` contém os métodos `PUT` para alterar os dados dos resíduos e `GET` para buscar os resíduos cadastrados.

## Testando

Infelizmente a parte de testes automatizados não consegui realizar. 
Tive algumas dificuldades em como realizar os testes usando leitura de arquivos.

